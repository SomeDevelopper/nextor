import nextcloud_client


def test_nextcloud_client_upload_delete():
    nc = nextcloud_client.Client('http://localhost:8080')
    nc.login('admin', 'admin')
    
    remote_file_path = 'test_nc_client.py'
    nc.put_file(remote_file_path, __file__)
    nc.delete(remote_file_path)
