from sha256sum import sha256sum


def test_sha256_similar_files_matching():
    file_1_sha256_sum = sha256sum('resources/Frog1-quality94.jpg')
    file_2_sha256_sum = sha256sum('resources/Frog2-quality94.jpg')

    assert file_1_sha256_sum == file_2_sha256_sum


def test_sha256_similar_files_different_qualities_not_matching():
    file_1_sha256_sum = sha256sum('resources/Frog1-quality94.jpg')
    file_2_sha256_sum = sha256sum('resources/Frog3-quality93.jpg')
    
    assert file_1_sha256_sum != file_2_sha256_sum


def test_sha256_different_files_not_matching():
    file_1_sha256_sum = sha256sum('resources/Frog1-quality94.jpg')
    file_2_sha256_sum = sha256sum('resources/Frog4_modified-quality94.jpg')
    
    assert file_1_sha256_sum != file_2_sha256_sum
    