import tkinter
from tkinter import ttk, Frame, Button

from nextcloud_client import Client as NcClient
import logging
from sha256sum import sha256sum
import shutil
import os


def compute_hash(cloud_client: NcClient, file_path: str) -> str:
    local_path = f"target{file_path}"  
        
    cloud_client.get_file(file_path, local_path)
    sha256 = sha256sum(local_path)
    
    return sha256


def compute_files_of(cloud_client: NcClient, dir_path: str) -> dict[str, str]:
    files = dict()
    
    local_dir_path = f"target{dir_path}"
    os.makedirs(local_dir_path, exist_ok=True)
    
    for file_info in cloud_client.list(dir_path):
        _path = file_info.path
        if file_info.is_dir():
            files.update(compute_files_of(cloud_client, _path))
            
        else:
            files[_path] = compute_hash(cloud_client, _path)
            
    shutil.rmtree(local_dir_path, ignore_errors=True)
    
    return files
   

def find_duplicates(files: dict[str, str]) -> None:
    hashes = dict()
    
    for _file_path, _file_hash in files.items():
        if _file_hash in hashes:
            hashes[_file_hash].append(_file_path)            
            logging.error(f"Alarm, the universe will collapse soon: {hashes[_file_hash]}")
        else:
            hashes[_file_hash] = [_file_path]


class NextorUi(Frame):
    def __init__(self):
        Frame.__init__(self)
        self.pack()
        self.master.title("Nextor helps you")
        self.button1 = Button(self, text="Help me!", width=25, command=self.find_duplicates)
        self.button1.grid( row = 0, column = 1, columnspan = 2, sticky = 'NSEW' )
        
    def find_duplicates(self):
        cloud_client = NcClient("http://localhost:8080")
        cloud_client.login('admin', 'admin')
        within_dir = ''
        
        file_hashes = compute_files_of(cloud_client, within_dir)
        logging.debug(file_hashes)
        find_duplicates(file_hashes)


def main():
    NextorUi().mainloop()


if __name__ == '__main__':
    logging.basicConfig(encoding='utf-8', level=logging.INFO)
    main()